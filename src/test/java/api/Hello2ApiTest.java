package api;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.core.Application;

import static org.junit.Assert.*;

public class Hello2ApiTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new ResourceConfig(Hello2Api.class);
    }

    @Test
    public void getHello() {
        final String hello = target("jax2").request().get(String.class);
        assertEquals("Hello JAX-RS2!", hello);
    }
}