package api;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.core.Application;

import static org.junit.Assert.*;

public class HelloApiTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new ResourceConfig(HelloApi.class);
    }

    @Test
    public void getHello() {
        final String hello = target("jax").request().get(String.class);
        assertEquals("Hello JAX-RS!", hello);
    }
}