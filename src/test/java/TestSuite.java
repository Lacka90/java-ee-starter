import api.Hello2ApiTest;
import api.HelloApiTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import servlet.WelcomeServletTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
    WelcomeServletTest.class,
    HelloApiTest.class,
    Hello2ApiTest.class
})

public class TestSuite {
}  	