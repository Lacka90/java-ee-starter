package servlet;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.PrintWriter;

import static org.junit.Assert.*;

public class WelcomeServletTest extends Mockito {
    @Test
    public void testServlet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        File file = new File("welcomeTest.txt");
        PrintWriter writer = new PrintWriter(file);
        when(response.getWriter()).thenReturn(writer);

        new WelcomeServlet().doGet(request, response);

        writer.flush();
        String content = FileUtils.readFileToString(new File("welcomeTest.txt"), "UTF-8");
        file.delete();
        assertTrue(content.contains("Hello servlet!"));
    }
}