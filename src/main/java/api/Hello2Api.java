package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/jax2")
public class Hello2Api {
    @GET
    public String getHello() {
        return "Hello JAX-RS2!";
    }
}
