package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/jax")
public class HelloApi {

    @GET
    public String getHello() {
        return "Hello JAX-RS!";
    }
}
