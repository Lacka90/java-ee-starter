package utils;

import java.util.Arrays;

public class EnvUtil {

    public static final String O52 = "O52";
    public static final String JAX_RS = "JAXRS";
    public static final String SERVLET = "SERVLET";
    public static final String STATIC = "STATIC";

    public static boolean isActive(String env) {
        String appType = System.getenv("APP_TYPE");
        String[] environments = appType.split(";");

        return Arrays.asList(environments).contains(env);
    }

    public static String get(String key) {
        return System.getenv(key);
    }
}
