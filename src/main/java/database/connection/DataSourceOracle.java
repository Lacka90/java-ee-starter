package database.connection;

import oracle.ucp.UniversalConnectionPoolException;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceOracle implements AbstractDatasource {
    private final static String POOL_NAME = "PUSH_POOL";
    private final static String CONN_FACTORY_CLASS_NAME = "oracle.jdbc.pool.OracleDataSource";
    private static AbstractDatasource database = null;
    private static PoolDataSource pds = null;

    private DataSourceOracle() throws ClassNotFoundException, SQLException, UniversalConnectionPoolException {
        String connectionUrl = "jdbc:oracle:thin:@localhost:49161:xe";
        String username = "system";
        String password = "oracle";

        pds = PoolDataSourceFactory.getPoolDataSource();

        pds.setConnectionFactoryClassName(CONN_FACTORY_CLASS_NAME);
        pds.setURL(connectionUrl);
        pds.setUser(username);
        pds.setPassword(password);
        pds.setConnectionPoolName(POOL_NAME);

        pds.setInitialPoolSize(5);
        pds.setMinPoolSize(5);
        pds.setMaxPoolSize(20);
    }

    public static AbstractDatasource getInstance() throws ClassNotFoundException, SQLException, UniversalConnectionPoolException {
        if (database == null) {
            database = new DataSourceOracle();
        }
        return database;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = pds.getConnection();
        conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        return conn;
    }

    public void close() {}
}
