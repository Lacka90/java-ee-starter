package database.connection;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public interface AbstractDatasource {
    public static AbstractDatasource getInstance() throws IOException, SQLException, PropertyVetoException, ClassNotFoundException {
        return null;
    }

    public Connection getConnection() throws SQLException;

    public void close();
}
