package database.connection;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceHikari implements AbstractDatasource {
    private static DataSourceHikari database = null;
    private HikariDataSource ds = null;

    private DataSourceHikari() throws ClassNotFoundException {
        String connectionUrl = "jdbc:oracle:thin:@localhost:49161:xe";
        String username = "system";
        String password = "oracle";

        Class.forName("oracle.jdbc.driver.OracleDriver");

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        hikariConfig.setJdbcUrl(connectionUrl);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setMinimumIdle(5);
        hikariConfig.setMaximumPoolSize(20);
        hikariConfig.setTransactionIsolation(String.valueOf(Connection.TRANSACTION_SERIALIZABLE));

        ds = new HikariDataSource(hikariConfig);
    }

    public static DataSourceHikari getInstance() throws ClassNotFoundException {
        if (database == null) {
            database = new DataSourceHikari();
        }
        return database;
    }

    public Connection getConnection() throws SQLException {
        return this.ds.getConnection();
    }

    public void close() {
        this.ds.close();
    }
}
