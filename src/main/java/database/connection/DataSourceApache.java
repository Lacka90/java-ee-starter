package database.connection;

import org.apache.commons.dbcp2.BasicDataSource;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceApache implements AbstractDatasource {
    private static DataSourceApache datasource;
    private BasicDataSource ds;

    private DataSourceApache() throws IOException, SQLException, PropertyVetoException, ClassNotFoundException {
        String connectionUrl = "jdbc:oracle:thin:@localhost:49161:xe";
        String username = "system";
        String password = "oracle";

        Class.forName("oracle.jdbc.driver.OracleDriver");

        ds = new BasicDataSource();
        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(connectionUrl);

        ds.setMinIdle(5);
        ds.setMaxIdle(20);
        ds.setDefaultTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
    }

    public static DataSourceApache getInstance() throws IOException, SQLException, PropertyVetoException, ClassNotFoundException {
        if (datasource == null) {
            datasource = new DataSourceApache();
        }
        return datasource;
    }

    public Connection getConnection() throws SQLException {
        return this.ds.getConnection();
    }

    public void close() {
        try {
            this.ds.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
