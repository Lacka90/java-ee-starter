package workers.o52;

import utils.EnvUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener()
public class O52Register implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        if (EnvUtil.isActive(EnvUtil.O52)) {
            System.out.println("O52 started!");
            Thread runner = new Thread(new O52Runner());
            runner.start();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {}
}
