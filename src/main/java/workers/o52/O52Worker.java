package workers.o52;

import database.connection.AbstractDatasource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class O52Worker implements Runnable {
    private boolean running = true;
    private AbstractDatasource ds;

    public O52Worker(AbstractDatasource ds) {
        this.ds = ds;
    }

    public void finish() {
        this.running = false;
    }

    @Override
    public void run() {
        while (running) {
            fetchDatabase();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void fetchDatabase() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT count(*) FROM app";
            conn = ds.getConnection();
            if (conn != null) {
                conn.setAutoCommit(false);
                ps = conn.prepareStatement(sql);
                ps.executeQuery();
                conn.commit();
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

