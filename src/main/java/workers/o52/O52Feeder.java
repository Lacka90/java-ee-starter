package workers.o52;

import database.connection.AbstractDatasource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class O52Feeder implements Runnable {
    private long id = 1;
    private AbstractDatasource ds;
    private int count = 0;
    private O52Worker worker = null;

    public O52Feeder(AbstractDatasource ds, int count, O52Worker worker) {
        this.ds = ds;
        this.count = count;
        this.worker = worker;
    }

    @Override
    public void run() {
        clearDatabase();
        long start = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            //if(i % 100 == 0) Logger.getInstance().info("Count: " + i);
            insertRecord();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("FINISHED: " + (System.currentTimeMillis() - start) + " ms");
        worker.finish();
    }

    private void clearDatabase() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM app";
            conn = ds.getConnection();
            if (conn != null) {
                conn.setAutoCommit(false);
                ps = conn.prepareStatement(sql);
                ps.executeQuery();
                conn.commit();
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void insertRecord() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            String sql = "INSERT INTO app (id, name) VALUES (SEQ_USER.NEXTVAL, ?)";
            conn = ds.getConnection();
            if (conn != null) {
                conn.setAutoCommit(false);
                ps = conn.prepareStatement(sql);;
                ps.setString(1, "name" + id++);
                ps.executeQuery();
                conn.commit();
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
