package workers.o52;

import database.connection.AbstractDatasource;
import database.connection.DataSourceApache;
import database.connection.DataSourceHikari;
import database.connection.DataSourceOracle;

public class O52Runner implements Runnable{

    @Override
    public void run() {
        try {
            AbstractDatasource ds = DataSourceApache.getInstance();
            O52Worker workerRunnable = new O52Worker(ds);
            Thread worker = new Thread(workerRunnable);
            O52Feeder feederRunnable = new O52Feeder(ds, 10000, workerRunnable);
            Thread feeder = new Thread(feederRunnable);
            worker.start();
            feeder.start();

            feeder.join();
            worker.join();

            ds.close();

            ds = DataSourceHikari.getInstance();
            workerRunnable = new O52Worker(ds);
            worker = new Thread(workerRunnable);
            feederRunnable = new O52Feeder(ds, 10000, workerRunnable);
            feeder = new Thread(feederRunnable);
            worker.start();
            feeder.start();

            feeder.join();
            worker.join();

            ds.close();

            ds = DataSourceOracle.getInstance();
            workerRunnable = new O52Worker(ds);
            worker = new Thread(workerRunnable);
            feederRunnable = new O52Feeder(ds, 10000, workerRunnable);
            feeder = new Thread(feederRunnable);
            worker.start();
            feeder.start();

            feeder.join();
            worker.join();

            ds.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
