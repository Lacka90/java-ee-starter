package servlet;

import org.glassfish.jersey.servlet.ServletContainer;
import utils.EnvUtil;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;

@WebListener()
public class ServletRegister implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        // Dynamic add servlet for servlet 3.x or later
        ServletContext sc = event.getServletContext();
        if (sc.getMajorVersion() >= 3) {
            if (EnvUtil.isActive(EnvUtil.SERVLET)) {
                final ServletRegistration.Dynamic welcomeServlet = sc.addServlet("WelcomeServlet", WelcomeServlet.class);
                welcomeServlet.setAsyncSupported(true);
                welcomeServlet.addMapping("/welcome");
            }

            if (EnvUtil.isActive(EnvUtil.STATIC)) {
                final ServletRegistration.Dynamic staticServlet = sc.addServlet("StaticServlet", StaticServlet.class);
                staticServlet.setInitParameter("basePath", "static");
                staticServlet.setAsyncSupported(true);
                staticServlet.addMapping("/*");
            }

            if (EnvUtil.isActive(EnvUtil.JAX_RS)) {
                final ServletRegistration.Dynamic jaxRs = sc.addServlet("app.Application", ServletContainer.class);
                jaxRs.setInitParameter("javax.ws.rs.Application", "app.Application");
                jaxRs.setInitParameter("javax.json.stream.JsonGenerator.prettyPrinting", "true");
                jaxRs.setLoadOnStartup(1);
                jaxRs.addMapping("/api/*");
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
