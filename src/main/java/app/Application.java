package app;

import api.Hello2Api;
import api.HelloApi;
import org.glassfish.jersey.server.ResourceConfig;
import utils.EnvUtil;

import javax.json.stream.JsonGenerator;

public class Application extends ResourceConfig {
    public Application() {
        if (EnvUtil.isActive(EnvUtil.JAX_RS)) {
            // Loading all resources in api package
            //packages("api");
            // Loading a specific Resource
            register(HelloApi.class);
            register(Hello2Api.class);
            property(JsonGenerator.PRETTY_PRINTING, true);
        }
    }
}
